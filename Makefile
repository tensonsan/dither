BUILDDIR = bin

SOURCES = dither.c
INCLUDES = 

OBJECTS = $(addprefix $(BUILDDIR)/, $(addsuffix .o, $(basename $(SOURCES))))

TARGET = $(BUILDDIR)/dither.so

CC = gcc
LD = gcc

CFLAGS = -O0 -g -Wall \
	-std=c99 \
	-fPIC -Wl,--no-undefined \
	$(INCLUDES)

LDFLAGS = -shared $(CFLAGS)

$(TARGET): $(OBJECTS)
	$(LD) $(LDFLAGS) -o $@ $(OBJECTS) $(LDLIBS) -lm -lc

$(BUILDDIR)/%.o: %.c
	mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS) $< -o $@

.PHONY: clean
clean:
	rm -rf $(BUILDDIR)
