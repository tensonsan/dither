
#include <string.h>
#include "dither.h"

#define IDX(i, j, w)    ((j) + (i) * (w))

static uint8_t saturate_add(uint8_t x, int8_t y)
{
    int16_t a = x;
    int16_t b = y;
    int16_t sum = a + b;

    if (sum > 255)
    {
        sum = 255;
    }
    else if (sum < 0)
    {
        sum = 0;
    }
    else
    {
    }

    return (uint8_t)sum;
}

void dither(uint8_t *buffer, uint32_t w, uint32_t h)
{
    for (uint32_t i=0; i < h; ++i)
    {
        for (uint32_t j=0; j < w; ++j)
        {
            uint8_t old = buffer[IDX(i, j, w)];
            uint8_t new = old > 127 ? 255 : 0;
            int16_t err = old - new;

            buffer[IDX(i, j, w)] = new;

            if ((i != h-1) && (j != 0) && (j != w - 1))
            {
                buffer[IDX(i,   j+1, w)] = saturate_add(buffer[IDX(i,   j+1, w)], (err * 7) / 16);
                buffer[IDX(i+1, j+1, w)] = saturate_add(buffer[IDX(i+1, j+1, w)], (err * 1) / 16);
                buffer[IDX(i+1, j,   w)] = saturate_add(buffer[IDX(i+1, j,   w)], (err * 5) / 16);
                buffer[IDX(i+1, j-1, w)] = saturate_add(buffer[IDX(i+1, j-1, w)], (err * 3) / 16);
            }
        }
    }
}

void dither_fast(uint8_t *in, uint8_t *out, uint32_t w, uint32_t h)
{
    uint8_t tmp[2 * w + 2];
    uint8_t *p = &tmp[1];   // Padding to avoid scratchpad buffer limit checking
    uint32_t next = 1;

    memset(tmp, 0, sizeof(tmp));
    memcpy(&p[0], &in[0], 2 * w);   // Copy the first two image lines into the scratchpad buffer

    for (uint32_t i=0; i < h; ++i)
    {
        for (uint32_t j=0; j < w; ++j)
        {
            uint8_t old = p[j];
            uint8_t new = old > 127 ? 255 : 0;
            int16_t err = old - new;

            p[j] = new;

            p[j + 1] = saturate_add(p[j + 1], (err * 7) / 16);
            p[j + 1 + w] = saturate_add(p[j + 1 + w], (err * 1) / 16);
            p[j + w] = saturate_add(p[j + w], (err * 5) / 16);
            p[j - 1 + w] = saturate_add(p[j - 1 + w], (err * 3) / 16);
        }

        memcpy(&out[i*w], &p[0], w);    // Copy result to from the scratchpad to the output buffer
        memcpy(&p[0], &p[w], w);        // TODO: can we do something to avoid this?

        if (next < h)
        {
            memcpy(&p[w], &in[next*w], w);  // Copy the next image line into the scratchpad buffer
            next++;
        }
    }
}
