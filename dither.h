
#ifndef _DITHER_H_
#define _DITHER_H_

#include <stdint.h>

void dither(uint8_t *buffer, uint32_t w, uint32_t h);
void dither_fast(uint8_t *in, uint8_t *out, uint32_t w, uint32_t h);

#endif
