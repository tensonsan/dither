#!/usr/bin/env python3

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import ctypes

IMG_PATH = 'grumpy.jpg'
LIB_PATH = './bin/dither.so'

def dither(lib, x):
    """Performs Floyd-Steinberg dithering using a C library

    Args:
        lib (string): C library path
        x (array): Grayscale image array

    Returns:
        array: Dithered image array
    """
    h = np.shape(x)[0]
    w = np.shape(x)[1]

    x = x.reshape(-1)

    c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_uint8))
    c_h = ctypes.c_uint32(h)
    c_w = ctypes.c_uint32(w)

    lib.dither(c_x, c_w, c_h)

    x = x.reshape(h, w)

    return x

def dither_fast(lib, x):
    """Performs Floyd-Steinberg dithering using a C library

    Args:
        lib (string): C library path
        x (array): Grayscale image array

    Returns:
        array: Dithered image array
    """
    h = np.shape(x)[0]
    w = np.shape(x)[1]

    x = x.reshape(-1)
    y = np.zeros(w * h, dtype=np.uint8)

    c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_uint8))
    c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_uint8))
    c_h = ctypes.c_uint32(h)
    c_w = ctypes.c_uint32(w)

    lib.dither_fast(c_x, c_y, c_w, c_h)

    y = y.reshape(h, w)

    return y

if __name__ == '__main__':

    # convert to grayscale
    img = Image.open(IMG_PATH).convert('L')
    imga = np.asarray(img)

    plt.figure('original')
    plt.imshow(imga, cmap="gray")

    # dither using C lib
    lib = ctypes.CDLL(LIB_PATH)
    imgd = dither_fast(lib, imga)

    plt.figure('dithering c')
    plt.imshow(imgd, cmap="gray")

    # dither using Python lib
    imgdp = img.convert('1', dither=Image.FLOYDSTEINBERG)
    imgdpa = np.asarray(imgdp)

    plt.figure('dithering pil')
    plt.imshow(imgdpa, cmap="gray")

    plt.show()
